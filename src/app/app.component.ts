import { Component } from '@angular/core';
import { Platform, LoadingController } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { DatabaseService } from './service/database.service';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent {
  constructor(private platform: Platform, private splashScreen: SplashScreen,
    private statusBar: StatusBar,
    private databaseService: DatabaseService,
    private loadingCtrl: LoadingController) {
      this.startApp()
    }
    async startApp(){
      this.platform.ready().then(async () => {
        const loading = await this.loadingCtrl.create();
        await loading.present();
        this.databaseService.init();
        this.databaseService.dbReady.subscribe(isReady => {
          if (isReady) {
            loading.dismiss();
            this.statusBar.styleDefault();
            this.splashScreen.hide();
          }
        });
      });
    }
}
