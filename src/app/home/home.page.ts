import { Component, ViewChild } from '@angular/core';
import { IonInfiniteScroll } from '@ionic/angular';
import { DatabaseService } from '../service/database.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  emp = [];
  dep = [];
  maxSal = [];
  depSelected: number | string = 'all';
  jai = "asd"
  pagenumber = 0;

  constructor(private dbService: DatabaseService) {
    this.loadDepartMent();

  }

  loadDepartMent() {
    this.dbService.getDept().subscribe(res => {
      this.dep = res.values;
      console.log("res1"+ this.dep);
      this.loadEmpData("all");
    })
  }
  changeDept(ev){
    console.log("selected depart:",ev.detail.value)
    this.depSelected = ev.detail.value;
    this.toggleInfiniteScroll();
    if(ev.detail.value == 'groupby'){
      this.loadHighestSalEmpData(ev.detail.value);
    }else{
      this.loadEmpData(ev.detail.value);
    }
  }

  loadEmpData(id: number | string) {
    if(this.emp.length >= 100){
      return;
    }
     this.dbService.getEmployee(id,this.pagenumber).subscribe(res => {
      this.emp.push(...res.values);
      console.log("res1"+ this.emp);
    })
  }
  loadHighestSalEmpData(id: number | string) {
    this.dbService.getEmployee(id,this.pagenumber).subscribe(res => {
     this.maxSal = res.values;
     console.log("res1"+ this.emp);
   })
 }

  loadData(event) {
    this.pagenumber++;
    console.log(this.pagenumber);
    this.loadEmpData(this.depSelected);
      setTimeout(() => {
        console.log('Done');
        event.target.complete();
        if (this.emp.length >= 100) {
        event.target.disabled = true;
        }
      }, 3000);

  }

  toggleInfiniteScroll() {
    if(this.depSelected === 'all'){
      this.infiniteScroll.disabled = true;
    }else if(this.depSelected == 'groupby' && this.emp.length < 100){
      this.infiniteScroll.disabled = false;
    }
  }


}
