import { Injectable } from '@angular/core';
import { Plugins } from '@capacitor/core';
import '@capacitor-community/sqlite';
import { JsonSQLite } from '@capacitor-community/sqlite';
import { BehaviorSubject, from, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { HttpClientModule } from '@angular/common/http';
import { AlertController } from '@ionic/angular';
const { CapacitorSQLite, Device, Storage } = Plugins;

const DB_SETUP_KEY = 'first_db_set';
const DB_NAME_KEY = 'db_name'
@Injectable({
  providedIn: 'root'
})
export class DatabaseService {
  dbReady = new BehaviorSubject(false);
  dbName = '';

  constructor(private http: HttpClientModule, private alertCtrl: AlertController) { }

  async init() {
    const info = await Device.getInfo();
    if(info.platform === 'android'){// because android required permission to save data
      try {
        const sqlite = CapacitorSQLite as any;
        await sqlite.requestPermissions();
        this.setupDataBase();

      } catch (error) {
        const alert = await this.alertCtrl.create({
          'header': 'No DB ACCESS',
          'message': 'This App cant work without access',
          'buttons': ['ok']
        })
        await alert.present();
      }
    }else{
      this.setupDataBase();
    }

  }

  private async setupDataBase() {
    const dbSetUpDone = await Storage.get({ key: DB_SETUP_KEY });
    if(!dbSetUpDone.value){
      this.downloadDatabase();
    }else {
      this.dbName = (await Storage.get({ key: DB_NAME_KEY })).value;
      await CapacitorSQLite.createConnection({database:this.dbName});
      await CapacitorSQLite.open({database: this.dbName});

      this.dbReady.next(true);
    }
  }
  private downloadDatabase(update = false) {
    fetch('./assets/data/db.json').then(res => res.json()).then(async (json: JsonSQLite) => {
      const jsonstring = JSON.stringify(json);
      const isvalid = await CapacitorSQLite.isJsonValid({ jsonstring });
      console.log("jsonstring",jsonstring);
      if(isvalid.result){
        this.dbName = json.database;
        await Storage.set({key: DB_NAME_KEY, value: this.dbName});
        await CapacitorSQLite.importFromJson({ jsonstring });
        await Storage.set({key: DB_SETUP_KEY, value:'1'})
        if(!update){
          await CapacitorSQLite.createSyncTable({database:this.dbName});
          await CapacitorSQLite.createConnection({database:this.dbName});
          await CapacitorSQLite.open({database: this.dbName});

          console.log("12");
        }else{
          await CapacitorSQLite.setSyncDate({syncdate: '' + new Date().getTime()})
          await CapacitorSQLite.createConnection({database:this.dbName});
          await CapacitorSQLite.open({database: this.dbName});
          console.log("21");
        }
        this.dbReady.next(true);
      }

    })
  }

  getDept(){
    return this.dbReady.pipe(
      switchMap(isReady => {
      if(!isReady){
        return of({values:[]})
      }else {
        const statement = 'SELECT * FROM department;';
        return from(CapacitorSQLite.query({database:this.dbName, statement,values:[]}));
      }
    })
    );
  }

   getEmployee(id: number | string, pageeNum : number) {
    //check db is ready first


    return this.dbReady.pipe(
      switchMap(isReady => {
        // console.log("isReady : ", isReady);
      if(!isReady){
        // console.log("isReady1 : ", isReady);
        return of({values:[]})
      }else {
        // console.log("isReady2 : ", isReady);
          let statement = 'SELECT a.Name as Name, a.Salary as Salary, b.Name as Dept FROM employess a JOIN department b on a.DeptId = b.ID and b.id = '+id+' ORDER BY a.Salary DESC;';
          if(id == "all"){
            let a = pageeNum * 10;
            statement = 'SELECT a.Name as Name, a.Salary as Salary, b.Name as Dept FROM employess a JOIN department b on a.DeptId = b.ID ORDER BY a.Salary DESC LIMIT '+a+',10;';
          }else if(id = "groupby"){
            statement = 'SELECT a.Name as Name, MAX(a.Salary) as Salary, b.Name as Dept FROM employess a JOIN department b on a.DeptId = b.ID group by Dept  ORDER BY a.Salary DESC;'
          }
        // const statement = 'SELECT * FROM employess;';
        return from(CapacitorSQLite.query({database:this.dbName, statement,values:[]}));
      }
    })
    );
  }
}
