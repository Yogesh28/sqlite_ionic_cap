(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["home-home-module"],{

/***/ "A3+G":
/*!*********************************************!*\
  !*** ./src/app/home/home-routing.module.ts ***!
  \*********************************************/
/*! exports provided: HomePageRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageRoutingModule", function() { return HomePageRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./home.page */ "zpKS");




const routes = [
    {
        path: '',
        component: _home_page__WEBPACK_IMPORTED_MODULE_3__["HomePage"],
    }
];
let HomePageRoutingModule = class HomePageRoutingModule {
};
HomePageRoutingModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forChild(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], HomePageRoutingModule);



/***/ }),

/***/ "WcN3":
/*!***************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/home/home.page.html ***!
  \***************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<ion-header [translucent]=\"true\" >\n  <ion-toolbar>\n    <ion-title>\n      SQLITE storage\n    </ion-title>\n  </ion-toolbar>\n</ion-header>\n<div ion-fixed>\n  <ion-item>\n    <ion-label>Select Your Choice</ion-label>\n    <ion-select value=\"all\" interface=\"action-sheet\" (ionChange)=\"changeDept($event)\">\n      <ion-select-option value=\"all\" >All</ion-select-option>\n      <ion-select-option value=\"groupby\" >Group By Department</ion-select-option>\n      <!-- <ion-select-option *ngFor=\"let a of dep\" value={{a?.ID}} >{{a?.Name}}</ion-select-option> -->\n    </ion-select>\n  </ion-item>\n  <ion-grid>\n    <ion-row class=\"header\">\n      <ion-col size-sm=\"4\">Name</ion-col>\n      <ion-col size-sm=\"4\">Department</ion-col>\n      <ion-col size-sm=\"4\">Salary</ion-col>\n    </ion-row>\n  </ion-grid>\n</div>\n<ion-content  class=\"ion-padding\">\n\n  <ion-grid *ngIf=\"depSelected == 'all'\">\n    <ion-row class=\"content\" *ngFor=\"let e of emp\">\n      <ion-col size-sm=\"4\">{{e?.Name}}</ion-col>\n      <ion-col size-sm=\"4\">{{e?.Dept}}</ion-col>\n      <ion-col size-sm=\"4\">{{e?.Salary}}</ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-grid *ngIf=\"depSelected == 'groupby'\">\n    <ion-row class=\"content\" *ngFor=\"let e of maxSal\">\n      <ion-col size-sm=\"4\">{{e?.Name}}</ion-col>\n      <ion-col size-sm=\"4\">{{e?.Dept}}</ion-col>\n      <ion-col size-sm=\"4\">{{e?.Salary}}</ion-col>\n    </ion-row>\n  </ion-grid>\n  <ion-infinite-scroll threshold=\"100px\" (ionInfinite)=\"loadData($event)\">\n    <ion-infinite-scroll-content\n      loadingSpinner=\"bubbles\"\n      loadingText=\"Loading more data...\">\n    </ion-infinite-scroll-content>\n  </ion-infinite-scroll>\n\n</ion-content>\n");

/***/ }),

/***/ "ct+p":
/*!*************************************!*\
  !*** ./src/app/home/home.module.ts ***!
  \*************************************/
/*! exports provided: HomePageModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePageModule", function() { return HomePageModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _home_page__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./home.page */ "zpKS");
/* harmony import */ var _home_routing_module__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./home-routing.module */ "A3+G");







let HomePageModule = class HomePageModule {
};
HomePageModule = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [
            _angular_common__WEBPACK_IMPORTED_MODULE_2__["CommonModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_4__["FormsModule"],
            _ionic_angular__WEBPACK_IMPORTED_MODULE_3__["IonicModule"],
            _home_routing_module__WEBPACK_IMPORTED_MODULE_6__["HomePageRoutingModule"]
        ],
        declarations: [_home_page__WEBPACK_IMPORTED_MODULE_5__["HomePage"]]
    })
], HomePageModule);



/***/ }),

/***/ "f6od":
/*!*************************************!*\
  !*** ./src/app/home/home.page.scss ***!
  \*************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = (".header {\n  text-align: center;\n  color: var(--ion-color-primary);\n  border: 1px solid var(--ion-color-primary);\n  padding: 8px;\n  border-radius: 5px;\n}\n\n.content {\n  text-align: center;\n  color: var(--ion-color-dark-shade);\n  border: 1px solid var(--ion-color-dark-shade);\n  padding: 10px;\n  border-radius: 5px;\n}\n\n[ion-fixed] {\n  width: 100%;\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uLy4uLy4uL2hvbWUucGFnZS5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUVBO0VBQ0Usa0JBQUE7RUFDQSwrQkFBQTtFQUNBLDBDQUFBO0VBQ0YsWUFBQTtFQUNBLGtCQUFBO0FBREE7O0FBR0E7RUFDQSxrQkFBQTtFQUNBLGtDQUFBO0VBQ0EsNkNBQUE7RUFDQSxhQUFBO0VBQ0Esa0JBQUE7QUFBQTs7QUFFQTtFQUNFLFdBQUE7QUFDRiIsImZpbGUiOiJob21lLnBhZ2Uuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIlxuXG4uaGVhZGVye1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGNvbG9yOiB2YXIoLS1pb24tY29sb3ItcHJpbWFyeSk7XG4gIGJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1wcmltYXJ5KTtcbnBhZGRpbmc6IDhweDtcbmJvcmRlci1yYWRpdXM6IDVweDtcbn1cbi5jb250ZW50e1xudGV4dC1hbGlnbjogY2VudGVyO1xuY29sb3I6IHZhcigtLWlvbi1jb2xvci1kYXJrLXNoYWRlKTtcbmJvcmRlcjogMXB4IHNvbGlkIHZhcigtLWlvbi1jb2xvci1kYXJrLXNoYWRlKTtcbnBhZGRpbmc6IDEwcHg7XG5ib3JkZXItcmFkaXVzOiA1cHg7XG59XG5baW9uLWZpeGVkXXtcbiAgd2lkdGg6IDEwMCU7XG5cbn1cbiJdfQ== */");

/***/ }),

/***/ "zpKS":
/*!***********************************!*\
  !*** ./src/app/home/home.page.ts ***!
  \***********************************/
/*! exports provided: HomePage */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomePage", function() { return HomePage; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "mrSG");
/* harmony import */ var _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! raw-loader!./home.page.html */ "WcN3");
/* harmony import */ var _home_page_scss__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home.page.scss */ "f6od");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _ionic_angular__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @ionic/angular */ "TEn/");
/* harmony import */ var _service_database_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../service/database.service */ "oWvL");






let HomePage = class HomePage {
    constructor(dbService) {
        this.dbService = dbService;
        this.emp = [];
        this.dep = [];
        this.maxSal = [];
        this.depSelected = 'all';
        this.jai = "asd";
        this.pagenumber = 0;
        this.loadDepartMent();
    }
    loadDepartMent() {
        this.dbService.getDept().subscribe(res => {
            this.dep = res.values;
            console.log("res1" + this.dep);
            this.loadEmpData("all");
        });
    }
    changeDept(ev) {
        console.log("selected depart:", ev.detail.value);
        this.depSelected = ev.detail.value;
        this.toggleInfiniteScroll();
        if (ev.detail.value == 'groupby') {
            this.loadHighestSalEmpData(ev.detail.value);
        }
        else {
            this.loadEmpData(ev.detail.value);
        }
    }
    loadEmpData(id) {
        if (this.emp.length >= 100) {
            return;
        }
        this.dbService.getEmployee(id, this.pagenumber).subscribe(res => {
            this.emp.push(...res.values);
            console.log("res1" + this.emp);
        });
    }
    loadHighestSalEmpData(id) {
        this.dbService.getEmployee(id, this.pagenumber).subscribe(res => {
            this.maxSal = res.values;
            console.log("res1" + this.emp);
        });
    }
    loadData(event) {
        this.pagenumber++;
        console.log(this.pagenumber);
        this.loadEmpData(this.depSelected);
        setTimeout(() => {
            console.log('Done');
            event.target.complete();
            if (this.emp.length >= 100) {
                event.target.disabled = true;
            }
        }, 500);
    }
    toggleInfiniteScroll() {
        if (this.depSelected === 'all') {
            this.infiniteScroll.disabled = true;
        }
        else if (this.depSelected == 'groupby' && this.emp.length < 100) {
            this.infiniteScroll.disabled = false;
        }
    }
};
HomePage.ctorParameters = () => [
    { type: _service_database_service__WEBPACK_IMPORTED_MODULE_5__["DatabaseService"] }
];
HomePage.propDecorators = {
    infiniteScroll: [{ type: _angular_core__WEBPACK_IMPORTED_MODULE_3__["ViewChild"], args: [_ionic_angular__WEBPACK_IMPORTED_MODULE_4__["IonInfiniteScroll"],] }]
};
HomePage = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_3__["Component"])({
        selector: 'app-home',
        template: _raw_loader_home_page_html__WEBPACK_IMPORTED_MODULE_1__["default"],
        styles: [_home_page_scss__WEBPACK_IMPORTED_MODULE_2__["default"]]
    })
], HomePage);



/***/ })

}]);
//# sourceMappingURL=home-home-module.js.map